/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2021, ITU/ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file     encmain.cpp
    \brief    Encoder application main
*/

#ifdef _MSC_VER
// this is to avoid error C2872
#ifndef _HAS_STD_BYTE
#define _HAS_STD_BYTE 0
#endif
#endif

#include <time.h>
#include <iostream>
#include <chrono>
#include <ctime>
#include <memory>

#include "EncoderLib/EncLibCommon.h"
#include "EncApp.h"
#include "Utilities/program_options_lite.h"

#ifdef _MSC_VER
// this is to avoid the macro defined in <WinGDI.h>, which is required by CImg
#undef ERROR
#endif

using namespace std;
using namespace vtm;

//! \ingroup EncoderApp
//! \{

static const uint32_t settingNameWidth = 66;
static const uint32_t settingHelpWidth = 84;
static const uint32_t settingValueWidth = 3;
// --------------------------------------------------------------------------------------------------------------------- //

//macro value printing function

#define PRINT_CONSTANT(NAME, NAME_WIDTH, VALUE_WIDTH) \
    cout << setw(NAME_WIDTH) << #NAME << " = " \
         << setw(VALUE_WIDTH) << NAME << '\n';

static void
printMacroSettings()
{
  if (g_verbosity >= DETAILS)
  {
    cout << "Non-environment-variable-controlled macros "
         << "set as follows: \n\n";

    //------------------------------------------------

    //setting macros

    PRINT_CONSTANT(RExt__DECODER_DEBUG_BIT_STATISTICS,
                   settingNameWidth, settingValueWidth);
    PRINT_CONSTANT(RExt__HIGH_BIT_DEPTH_SUPPORT,
                   settingNameWidth, settingValueWidth);
    PRINT_CONSTANT(RExt__HIGH_PRECISION_FORWARD_TRANSFORM,
                   settingNameWidth, settingValueWidth);
    PRINT_CONSTANT(ME_ENABLE_ROUNDING_OF_MVS,
                   settingNameWidth, settingValueWidth);

    //------------------------------------------------

    cout << '\n';
  }
}

// ====================================================================================================================
// Main function
// ====================================================================================================================

int
main(int argc, char *argv[])
{
  // print information
  fprintf(stdout, "\n");
  fprintf(stdout, "VVCSoftware: VTM Encoder Version %s ", VTM_VERSION);
  fprintf(stdout, NVM_ONOS);
  fprintf(stdout, NVM_COMPILEDBY);
  fprintf(stdout, NVM_BITS);
#if ENABLE_SIMD_OPT
  string SIMD;
  df::program_options_lite::Options opts;
  opts.addOptions()
    ("SIMD", SIMD, string( "" ), "")
    ("c", df::program_options_lite::parseConfigFile, "");
  df::program_options_lite::SilentReporter err;
  df::program_options_lite::scanArgv(opts, argc, (const char **) argv, err);
  fprintf(stdout, "[SIMD=%s] ", read_x86_extension(SIMD));
#endif
#if ENABLE_TRACING
  fprintf(stdout, "[ENABLE_TRACING] ");
#endif
#if ENABLE_SPLIT_PARALLELISM
  fprintf(stdout, "[SPLIT_PARALLEL (%d jobs)]", PARL_SPLIT_MAX_NUM_JOBS);
#endif
#if ENABLE_SPLIT_PARALLELISM
  const char *waitPolicy = getenv("OMP_WAIT_POLICY");
  const char *maxThLim = getenv("OMP_THREAD_LIMIT");
  fprintf(stdout,
          waitPolicy ? "[OMP: WAIT_POLICY=%s," : "[OMP: WAIT_POLICY=,",
          waitPolicy);
  fprintf(stdout, maxThLim ? "THREAD_LIMIT=%s" : "THREAD_LIMIT=", maxThLim);
  fprintf(stdout, "]");
#endif
  fprintf(stdout, "\n");

  fstream bitstream;
  EncLibCommon encLibCommon;

  vector<unique_ptr<EncApp>> pcEncApp(1);
  bool resized = false;
  int layerIdx = 0;

  initROM();
  TComHash::initBlockSizeToIndex();

  unique_ptr<char *[]> layerArgvPtr = make_unique<char *[]>(argc);

  do
  {
    pcEncApp[layerIdx] = make_unique<EncApp>(bitstream, &encLibCommon);
    // create application encoder class per layer
    pcEncApp[layerIdx]->create();

    // parse configuration per layer
    try
    {
      char **layerArgv = layerArgvPtr.get();
      int j = 0;
      for (int i = 0; i < argc; i++)
      {
        if (argv[i][0] == '-' && argv[i][1] == 'l')
        {
          if (argc <= i + 1)
          {
            THROW("Command line parsing error: "
                  "missing parameter after -lx\n");
          }
          int numParams = 1; // count how many parameters are consumed
          // check for long parameters, which start with "--"
          const string param = argv[i + 1];
          if (param.rfind("--", 0) != 0)
          {
            // only short parameters have a second parameter for the value
            if (argc <= i + 2)
            {
              THROW("Command line parsing error: "
                    "missing parameter after -lx\n");
            }
            numParams++;
          }
          // check if correct layer index
          if (argv[i][2] == to_string( layerIdx ).c_str()[0])
          {
            layerArgv[j] = argv[i + 1];
            if (numParams > 1)
            {
              layerArgv[j + 1] = argv[i + 2];
            }
            j+= numParams;
          }
          i += numParams;
        }
        else
        {
          layerArgv[j] = argv[i];
          j++;
        }
      }

      if (!pcEncApp[layerIdx]->parseCfg(j, layerArgv))
      {
        pcEncApp[layerIdx]->destroy();
        return 1;
      }
    }
    catch (df::program_options_lite::ParseFailure &e)
    {
      cerr << "Error parsing option \"" << e.arg
           << "\" with argument \"" << e.val << "\".\n";
      return 1;
    }

    pcEncApp[layerIdx]->createLib(layerIdx);

    if (!resized)
    {
      pcEncApp.resize(pcEncApp[layerIdx]->getMaxLayers());
      resized = true;
    }

    layerIdx++;
  } while (layerIdx < pcEncApp.size());

  if (layerIdx > 1)
  {
    VPS *vps = pcEncApp[0]->getVPS();
    //check chroma format and bit-depth for dependent layers
    for (uint32_t i = 0; i < layerIdx; i++)
    {
      int curLayerChromaFormatIdc = pcEncApp[i]->getChromaFormatIDC();
      int curLayerBitDepth = pcEncApp[i]->getBitDepth();
      for (uint32_t j = 0; j < layerIdx; j++)
      {
        if (vps->getDirectRefLayerFlag(i, j))
        {
          int refLayerChromaFormatIdcInVPS = pcEncApp[j]->getChromaFormatIDC();
          CHECK(curLayerChromaFormatIdc != refLayerChromaFormatIdcInVPS,
                "The chroma formats of the current layer and the reference "
                "layer are different");
          int refLayerBitDepthInVPS = pcEncApp[j]->getBitDepth();
          CHECK(curLayerBitDepth != refLayerBitDepthInVPS,
                "The bit-depth of the current layer and the reference layer "
                "are different");
        }
      }
    }
  }

#if PRINT_MACRO_VALUES
  printMacroSettings();
#endif

  // starting time
  auto startTime = chrono::steady_clock::now();
  time_t startTime2 =
    chrono::system_clock::to_time_t(chrono::system_clock::now());
  fprintf(stdout, " started @ %s", ctime(&startTime2));
  clock_t startClock = clock();

  // call encoding function per layer
  bool eos = false;

  while (!eos)
  {
    // read GOP
    bool keepLoop = true;
    while (keepLoop)
    {
      for (auto &encApp : pcEncApp)
      {
#ifndef _DEBUG
        try
        {
#endif
          keepLoop = encApp->encodePrep(eos);
#ifndef _DEBUG
        }
        catch (Exception &e)
        {
          cerr << e.what() << '\n';
          return EXIT_FAILURE;
        }
        catch (const bad_alloc &e)
        {
          cout << "Memory allocation failed: " << e.what() << '\n';
          return EXIT_FAILURE;
        }
#endif
      }
    }

    // encode GOP
    keepLoop = true;
    while (keepLoop)
    {
      for (auto &encApp : pcEncApp)
      {
#ifndef _DEBUG
        try
        {
#endif
          keepLoop = encApp->encode();
#ifndef _DEBUG
        }
        catch (Exception &e)
        {
          cerr << e.what() << '\n';
          return EXIT_FAILURE;
        }
        catch (const bad_alloc &e)
        {
          cout << "Memory allocation failed: " << e.what() << '\n';
          return EXIT_FAILURE;
        }
#endif
      }
    }
  }
  // ending time
  clock_t endClock = clock();
  auto endTime = chrono::steady_clock::now();
  time_t endTime2 =
    chrono::system_clock::to_time_t(chrono::system_clock::now());
#if JVET_O0756_CALCULATE_HDRMETRICS
  auto metricTime = pcEncApp[0]->getMetricTime();

  for (int layerIdx = 1; layerIdx < pcEncApp.size(); layerIdx++)
  {
    metricTime += pcEncApp[layerIdx]->getMetricTime();
  }
  auto totalTime =
    chrono::duration_cast<chrono::milliseconds>(endTime - startTime).count();
  auto encTime =
    chrono::duration_cast<chrono::milliseconds>(endTime - startTime - metricTime).count();
  auto metricTimeuser =
    chrono::duration_cast<chrono::milliseconds>(metricTime).count();
#else
  auto encTime =
    chrono::duration_cast<chrono::milliseconds>(endTime - startTime).count();
#endif

  for (auto &encApp : pcEncApp)
  {
    encApp->destroyLib();

    // destroy application encoder class per layer
    encApp->destroy();
  }

  // destroy ROM
  destroyROM();

  pcEncApp.clear();

  printf("\n finished @ %s", ctime(&endTime2));

#if JVET_O0756_CALCULATE_HDRMETRICS
  printf(" Encoding Time (Total Time): %12.3f ( %12.3f ) sec."
         " [user] %12.3f ( %12.3f ) sec. [elapsed]\n",
         ((endClock - startClock) * 1.0 / CLOCKS_PER_SEC) -
         (metricTimeuser / 1000.0),
         (endClock - startClock) * 1.0 / CLOCKS_PER_SEC,
         encTime / 1000.0, totalTime / 1000.0);
#else
  printf(" Total Time: %12.3f sec. [user] %12.3f sec. [elapsed]\n",
         (endClock - startClock) * 1.0 / CLOCKS_PER_SEC, encTime / 1000.0);
#endif

  return 0;
}

//! \}
